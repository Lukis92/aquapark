class UpdateForeignKeyForIndividualTrainings < ActiveRecord::Migration
  def change
    remove_column :individual_trainings, :training_cost_id
    add_reference :individual_trainings, :training_cost, index: true
    add_foreign_key :individual_trainings, :training_costs
  end
end
