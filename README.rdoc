== What's done:
* usunięty problem z duplikatami treningów indywidualnych
* smooth scrolling na stronie głównej
* poprawiony tekst na stronie głównej
* li active dla poszczególnych sekcji strony głównej
* obsłużone nil dla statistics/news
* wyłączenie znikających alertów na bought_details/new
* poprawiony widok formularza rejestracji pracowników
* wyłączenie znikających alertów w individual_trainings/new
* li active dla podstron 'Logowanie', 'Rejestracja'
* naprawienie przyleganie footer do dołu
* naprawiona aktualizacja i walidacja hasła w profilu użytkownika
* rejestrować się mogą tylko osoby powyżej 16 roku życia
* możliwość zmiany typu pracownika przez managera
* naprawienie resetowania hasła użytkownika
* usuwanie individualnych treningów, aktywności podczas zamykania konta klienta, trenera
* gdy usuwam cennik indywidualnych treningów to indywidualne treningi pozostają w bazie
* podgląd ostatnich 10 imion osób, które wystawiły like pod artykułem
* edycja treningu indywidualnego przez managera
* możliwość przydzielania treningu przez managera
