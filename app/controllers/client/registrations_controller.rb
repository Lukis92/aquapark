# Controller For  Client registration actions by devise gem
class Client::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :require_no_authentication, only: [:new, :create]
  before_action :configure_permitted_parameters, only: [:create, :update]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    super
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   if current_person.update_with_password(params[:person])
  #     sign_out(current_person)
  #     sign_in(:person, current_client, bypass: true)
  #     flash[:notice] = 'Hasło zostało zmienione.'
  #     redirect_to backend_person_path(current_person)
  #   else
  #     render :edit
  #   end
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.for(:sign_up) << :attribute
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.for(:account_update) << :attribute
  # end

  # The path used after sign up.
  def after_sign_up_path_for(_resource)
    # super(resource)
    new_person_session_path
  end
  #
  # def after_sign_in_path_for(resource)
  # end
  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  protected

  def update_resource(resource, params)
    resource.update_without_password(params) if current_manager
  end

  def after_update_path_for(resource)
    backend_person_path(resource)
  end

  private

  def configure_permitted_parameters
    sign_up_sanitized_params = %i(
      pesel
      first_name
      last_name
      date_of_birth
      profile_image
      password
      password_confirmation
    )

    update_sanitized_params = %i(
      pesel
      first_name
      last_name
      date_of_birth
      profile_image
      password
      password_confirmation
      current_password
    )

    devise_parameter_sanitizer.permit(:sign_up, keys: sign_up_sanitized_params)
    devise_parameter_sanitizer.permit(:account_update, keys: update_sanitized_params)
  end
end
